package org.niit.app

import org.niit.common.TApp
import org.niit.controller.{EDUDataController, MovieDataController}

object EDUDataApp extends App with TApp{

  start("local[*]","eduData"){


    val eduDataContoller = new EDUDataController
    eduDataContoller.dispatch()

  }

}
