package org.niit.app

import org.apache.spark.streaming.dstream.DStream
import org.niit.common.TApp
import org.niit.controller.{BlackListController, RealTimeController, TimeCountController}
import org.niit.handler.DataHandler
import org.niit.bean.AdClickData

object BlackListApp extends App with TApp{

  start("local[*]","BlackList"){
    val blackListController = new BlackListController()
    val realTimeController = new RealTimeController()
    val timeCountController = new TimeCountController()

    val data = DataHandler.kafkaAdDataHandler("BD2", "AD2")

    timeCountController.dispatch(data)
    realTimeController.dispatch(data)
    blackListController.dispatch(data)
    DataHandler.startAndAwait()





  }

}
