package org.niit.app

import org.niit.app.BlackListApp.start
import org.niit.common.TApp
import org.niit.controller.{BlackListController, MovieDataController, RealTimeController, TimeCountController}
import org.niit.handler.DataHandler

object MovieDataApp extends App with TApp{

  start("local[*]","MovieData"){


    val movieDataController = new MovieDataController
    movieDataController.dispatch()

  }

}
