package org.niit.dao

import org.niit.common.TRealTimeDao
import org.niit.util.JDBCUtil

class RealTimeDao extends TRealTimeDao{
  override def insertRealTimeAd(day: String, area: String, city: String, ad: String, count: String): Unit ={
    //1.获得MySQL连接
    val conn =  JDBCUtil.getConnection
    //2.编写并执行MySQL语句，以及传递参数
    JDBCUtil.executeUpdate(conn,
      """
        |INSERT INTO
        |         area_city_ad_count(dt,area,city,adid,count)
        |VALUES (?,?,?,?,?)
        |ON DUPLICATE KEY UPDATE
        | count = count + ?
        |""".stripMargin,Array(day,area,city,ad,count,count))
    //3.关闭连接
    conn.close()
  }
}
