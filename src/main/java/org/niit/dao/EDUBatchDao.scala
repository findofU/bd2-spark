package org.niit.dao

import java.util.Properties

import org.apache.spark.sql.Dataset
import org.niit.bean.AnswerWithRecommendations
import org.niit.util.SparkUtil

class EDUBatchDao {

  private val spark = SparkUtil.takeSpark()
  import spark.implicits._

  //获取edu表中的数据
  def getEDUData(): Dataset[AnswerWithRecommendations] ={
    val props = new Properties()
    props.setProperty("user","root")
    props.setProperty("password","Niit@123")
    val allInfoDS:Dataset[AnswerWithRecommendations] =  spark.read.jdbc(
      "jdbc:mysql://node1:3306/BD1?useUnicode=true&characterEncoding=utf8",
      "edu",
      props
    ).as[AnswerWithRecommendations]
    //返回值
    allInfoDS
  }

}
