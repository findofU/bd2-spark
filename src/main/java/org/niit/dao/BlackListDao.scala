package org.niit.dao

import org.apache.spark.sql.DataFrame
import org.niit.common.TDao
import org.niit.bean.AdClickData
import org.niit.util.JDBCUtil

class BlackListDao extends TDao{

  override def selectBlackUserById(userid: String): Boolean = {
    //1.获得MySQL连接
    val conn = JDBCUtil.getConnection
    //2.编写需要执行的SQL语句，以及参数
    val bool = JDBCUtil.isExist(conn,
      """SELECT
        |     *
        | FROM
        |   black_list
        | WHERE
        |   userid = ?
        |""".stripMargin,Array(userid))
    //3.关闭连接
    conn.close()
    //4.返回值
    bool
  }

  override def insertBlackList(userid: String): Unit = {
    //1.获得MySQL连接
    val conn = JDBCUtil.getConnection
    //2.编写需要执行的SQL语句，以及参数
    JDBCUtil.executeUpdate(conn,
      """
        |INSERT INTO
        |   black_list
        |         (userid)
        |VALUES
        |           (?)
        |ON DUPLICATE KEY UPDATE
        |            userid = ?
        |""".stripMargin,Array(userid,userid))
    //3.关闭连接
    conn.close()
  }

  override def insertUserAdCount(day:String, user:String, ad:String,count:Int ): Unit = {
    //1.获得MySQL连接
    val conn = JDBCUtil.getConnection
    //2.编写需要执行的SQL语句，以及参数
    JDBCUtil.executeUpdate(conn,
      """
        |INSERT INTO
        |     user_ad_count(dt,userid,adid,count)
        |     VALUES (?,?,?,?)
        |ON DUPLICATE KEY UPDATE
        |       count = count + ?
        |""".stripMargin,Array(day,user,ad,count,count))
    //3.关闭连接
    conn.close()
  }

  def checkUserByCount(day:String,user:String,ad:String,count:Int):Boolean={
    //1.获得MySQL连接
    val conn = JDBCUtil.getConnection
    //2.编写需要执行的SQL语句，以及参数
    val bool = JDBCUtil.isExist(conn,
      """
        |SELECT
        |       *
        |FROM
        |     user_ad_count
        |WHERE
        |     dt = ?
        |AND
        |     userid = ?
        |AND
        |     adid = ?
        |AND
        |     count >= ?
        |""".stripMargin,Array(day,user,ad,count))
    //3.关闭连接
    conn.close()
    //4.返回值
    bool
  }
}
