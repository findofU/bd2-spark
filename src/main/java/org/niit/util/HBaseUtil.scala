package org.niit.util

import java.util

import org.apache.hadoop.hbase.{Cell, HBaseConfiguration, TableName}
import org.apache.hadoop.hbase.client.{Connection, ConnectionFactory, Get, Put, Table}
import org.apache.hadoop.hbase.util.Bytes

object HBaseUtil {

  private val conn = init()

  private var hTable:Table = _;

  private def init(): Connection ={
    val conf = HBaseConfiguration.create()
    conf.set("hbase.zookeeper.quorum","node1")
    val connection = ConnectionFactory.createConnection(conf)
    connection
  }

  def getConnection:Connection={
    conn
  }
  def setHTable(tableName:String):Table={
    val hbaseTableName: TableName = TableName.valueOf(tableName)
    val hbaseTable = conn.getTable(hbaseTableName)
    hTable = hbaseTable
    hbaseTable
  }

  def putData(put: Put): Unit ={
    hTable.put(put)
    hTable.close()
    conn.close()
  }

  def putDatas(puts:java.util.List[Put]): Unit ={

    hTable.put(puts)
    hTable.close()
    conn.close()
  }

  def getData(get:Get): List[String] ={
    var value = List[String]()
    val result = hTable.get(get)
    val cells: util.List[Cell] = result.listCells()
    cells.forEach(c=>{
      var res = Bytes.toString(c.getValueArray,c.getValueOffset,c.getValueLength)
      value = value:+res
    })
    value
  }

  def main(args: Array[String]): Unit = {
    HBaseUtil.setHTable("bigdata:student")
    //val puts = new util.ArrayList[Put]()
//    val put: Put = new Put(Bytes.toBytes("yyy1111"))
//    put.addColumn(Bytes.toBytes("info"), Bytes.toBytes("age"), Bytes.toBytes("20"))
//    //puts.add(put)
//    HBaseUtil.putData(put)
     val get = new Get(Bytes.toBytes("yyy1111"))
    val value = HBaseUtil.getData(get)
    println(value)

  }

}
