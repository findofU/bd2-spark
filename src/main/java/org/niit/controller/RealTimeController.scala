package org.niit.controller

import org.apache.spark.streaming.dstream.DStream
import org.niit.common.TController
import org.niit.bean.AdClickData
import org.niit.service.RealTimeService

class RealTimeController extends TController{

  private val realTimeService = new RealTimeService

  override def dispatch(data:DStream[AdClickData]): Unit = {

    realTimeService.dataAnalysis(data)


  }
}
