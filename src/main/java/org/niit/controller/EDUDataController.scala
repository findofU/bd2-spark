package org.niit.controller

import org.niit.handler.DataHandler
import org.niit.service.{EDUBatchService, EDUDataService, EDURecommendService}

class EDUDataController {

  private val eduDataService = new EDUDataService()
  private val eduRecommendService = new EDURecommendService()
  private val eduBatchService = new EDUBatchService()
  def dispatch(): Unit ={

//    val answerData = DataHandler.kafkaAnswerDataHandler("BD2","edu2")
//    eduDataService.dataAnalysis(answerData)
//    eduRecommendService.dataAnalysis(answerData)
//
//    DataHandler.startAndAwait()

    eduBatchService.dataAnalysis()
  }

}
