package org.niit.controller

import java.io.{FileWriter, PrintWriter}

import org.apache.spark.streaming.dstream.DStream
import org.niit.common.TController
import org.niit.bean.AdClickData
import org.niit.service.TimeCountService

class TimeCountController extends TController{

  private val timeCountService = new TimeCountService

  override def dispatch(data: DStream[AdClickData]): Unit = {
   timeCountService.dataAnalysis(data)

  }
}
