package org.niit.controller

import org.apache.spark.streaming.dstream.DStream
import org.niit.bean.AdClickData
import org.niit.service.MovieDataService

class MovieDataController {

  private val movieDataService = new MovieDataService

  def dispatch(): Unit = {
    movieDataService.dataAnalysis(null)
  }

}
