package org.niit.controller

import org.apache.spark.streaming.dstream.DStream
import org.niit.common.TController
import org.niit.bean.AdClickData
import org.niit.service.BlackListService

class BlackListController extends TController{

  private val blackListService = new BlackListService

  override def dispatch(data:DStream[AdClickData]): Unit = {
    blackListService.dataAnalysis(data)
  }
}
