package org.niit.bean
//真实环境：日志-->Flume-->Kafka-->Spark-->DB
//将 Kafka中模拟的用户点击广告数据封装进AdClickData
case class AdClickData(ts:String, area:String, city:String, user:String, ad:String) extends java.io.Serializable
