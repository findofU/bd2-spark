package org.niit.bean

import java.sql.Timestamp

/**
 * 学生答题信息样例类
 * {
 * "student_id":"学生ID_31",
 * "textbook_id":"教材ID_1",
 * "grade_id":"年级ID_6",
 * "subject_id":"科目ID_2_语文",
 * "chapter_id":"章节ID_chapter_3",
 * "question_id":"题目ID_1003",
 * "score":7,
 * "answer_time":"2021-01-09 14:53:28",
 * "ts":"Jan 9, 2021 2:53:28 PM"
 * }
 *
 *  转换的过程叫做映射 因为 键名和 属性名相同，所以可以将值赋值给属性当中
 *  约定 优于 编码
 */
case class Answer(student_id: String, //学生ID
                  textbook_id: String, //教材ID
                  grade_id: String, //年级ID
                  subject_id: String, //科目ID
                  chapter_id: String,//章节ID
                  question_id: String,//题目ID
                  score: Int,//题目得分，0~10分
                  answer_time: String,//答题提交时间，yyyy-MM-dd HH:mm:ss字符串形式
                  ts: Timestamp //答题提交时间，时间戳形式
                 ) extends Serializable