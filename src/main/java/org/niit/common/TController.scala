package org.niit.common

import org.apache.spark.streaming.dstream.DStream
import org.niit.bean.AdClickData

trait TController {

  def dispatch(data:DStream[AdClickData]):Unit

}
