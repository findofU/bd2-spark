package org.niit.common

import org.niit.util.SparkUtil
import org.apache.spark.SparkConf
import org.apache.spark.streaming.Seconds
import org.niit.handler.DataHandler

trait TApp {

  def start(master:String = "local[*]",appName:String = "application")(op : => Unit ) : Unit = {

    val sparkConf = new SparkConf().setMaster(master).setAppName(appName)

    val sc = SparkUtil.CreateSpark(sparkConf,Seconds(5))
    val spark = SparkUtil.takeSpark()
    val ssc = SparkUtil.takeSSC()
    sc.setLogLevel("ERROR")


    try{
      op
    }catch {
      case ex => ex.printStackTrace()
    }


    spark.stop()
    ssc.stop(true,true)
    sc.stop()
    SparkUtil.clear()

  }

}
