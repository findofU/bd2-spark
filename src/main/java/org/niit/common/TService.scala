package org.niit.common

import org.niit.util.SparkUtil
import org.apache.spark.sql.SparkSession
import org.apache.spark.streaming.dstream.DStream
import org.niit.bean.AdClickData

trait TService {


  def dataAnalysis(data:DStream[AdClickData]):Any

}
