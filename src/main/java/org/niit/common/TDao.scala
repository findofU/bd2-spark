package org.niit.common

import org.niit.util.SparkUtil
import org.niit.bean.AdClickData
import org.niit.util.JDBCUtil

trait TDao {

  val sc = SparkUtil.takeSC()

  //通过Id查找黑名单用户
  def selectBlackUserById(userid:String):Boolean
  //向黑名单表添加数据
  def insertBlackList(userid: String):Unit
  //向 每日用户点击广告次数表 添加数据
  def insertUserAdCount(day:String, user:String, ad:String,count:Int):Unit
  //根据 次数 查询用户是否超过规定次数
  def checkUserByCount(day:String,user:String,ad:String,count:Int):Boolean

}
