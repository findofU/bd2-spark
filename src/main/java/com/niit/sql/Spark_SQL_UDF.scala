package com.niit.sql

import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession

object Spark_SQL_UDF {

  def main(args: Array[String]): Unit = {
    val sparkConf = new SparkConf().setMaster("local[*]").setAppName("sparkSQL")
    val spark = SparkSession.builder().config(sparkConf).getOrCreate()
    import spark.implicits._

    val df =  spark.read.json("input/user.json")
    df.createOrReplaceTempView("user")
                          // 方法名      //方法逻辑
    spark.udf.register("prefix",(name:String)=>{
      "Name:"+name
    })

    spark.sql("select age,prefix(username)  from user").show() //Name:zhangsan

    spark.close()

  }

}
