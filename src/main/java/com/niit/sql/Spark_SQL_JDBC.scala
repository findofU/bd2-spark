package com.niit.sql

import org.apache.spark.SparkConf
import org.apache.spark.sql.{SaveMode, SparkSession}

object Spark_SQL_JDBC {

  def main(args: Array[String]): Unit = {
    //准备环境
    val sparkConf = new SparkConf().setMaster("local[*]").setAppName("sparkSQL")
    val spark = SparkSession.builder().config(sparkConf).getOrCreate();

    //1.读取MySQL数据

    val df = spark.read //转换成了一个DataFrame
        .format("jdbc")//数据库链接源 jdbc 表示MySQL
        .option("url","jdbc:mysql://node1:3306/BD2")//MySQL的连接地址 地址：node1 端口号:3306  数据库：BD2
        .option("driver","com.mysql.jdbc.Driver")//MySQL的链接驱动  com.mysql.jdbc.Driver
        .option("user","root")  //数据库的用户名
        .option("password","Niit@123") //数据库的密码
        .option("dbtable","User")//表 表名是你们自己的
        .load()
    df.show()
  //将DataFrame写入MySQL
   df.write
      .format("jdbc")
      .option("url","jdbc:mysql://node1:3306/BD2")
      .option("driver","com.mysql.jdbc.Driver")
      .option("user","root")
      .option("password","Niit@123")
      .option("dbtable","User2")//写到User2表里面
      .mode(SaveMode.Append)//追加模式，如果该表不存在就会自动的创建
      .save()

    spark.close();
  }

}
