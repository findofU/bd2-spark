package com.niit.sql

import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession

object Spark_SQL_Base01 {

  def main(args: Array[String]): Unit = {
    //准备环境
    val sparkConf = new SparkConf().setMaster("local[*]").setAppName("sparkSQL")
    val spark = SparkSession.builder().config(sparkConf).getOrCreate();

    val df =  spark.read.json("input/user.json")
    df.show()

    //DataFrame ==> SQL
    df.createOrReplaceTempView("user")
    spark.sql("select * from user").show()
    //计算年龄平均值
    spark.sql("select avg(age) from user").show()

    //DataFrame ==> DSL
    df.select("age","username").show()
    import spark.implicits._//在使用DataFrame时候，如果涉及道转换的操作，需要引入转换规则
    //查询所有年龄  并 +1
    df.select($"age" +1 as "newAge").show

    //DataSet
    val seq = Seq(1,2,3,4)
    val ds =  seq.toDS();
    ds.show()

    //RDD 《==》 DataFrame
    val rdd = spark.sparkContext.makeRDD( List( (1,"zhangsan",30 ),(2,"lisi",40 ) ) )
    val df1 =  rdd.toDF("id","name","age")
    val rdd1 =  df1.toDF()

    //DataSet 《==》 DataFrame
    val ds1 = df1.as[User]
    val df2 = ds1.toDF();  //三者转换 只要是转换为DataFrame都是toDF.

    //RDD 《==》 DataSet
    val ds2 = rdd.map{
        case (id,name,age) =>{
          User(id,name,age)
        }
      }.toDS()

    val rdd2 = ds2.rdd


    spark.close()

  }
  case class User(id:Int,name:String,age:Int)

}
